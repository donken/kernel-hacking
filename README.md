This assignment was based around hacking kernel modules (i.e. Device
drivers) to do certain things. The first driver is supposed to sleep
on a write and awake after a certain amount of time or it is written to.

The second is built to hack the system call table and insert malicious code
into the execution logic for Unix-based systems such that we can spy
on Open Syscalls. It also cleans up if it is uninstalled so that the Syscall table
returns to normal.

Author: David Onken